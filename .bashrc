
#
# Bourne Again SHell init file.
#

# Do global configuration.
ARCH=`uname -s`

# Set the default permission for new files to rwxr-xr-x.
umask 022

# Set the printer queue.
export PRINTER=All_Clusters
# export PRINTER=all_clusters_d  # For double-sided printing.
# export PRINTER=all_clusters_s  # For single-sided printing.

# Set the terminal type so applications (e.g. emacs) use color.
#export TERM=xterm-color

# set prefered editor
export EDITOR=vim

# set the PATH environmental variable
#export PATH=/usr/local/bin:$PATH
#export PATH=/usr/local/bin:$PATH
export PATH=$PATH:/usr/bin/meminfo
export PATH=/usr/local/texlive/2013/bin/x86_64-darwin:$PATH
export PATH=$PATH:/opt/homebrew-cask/Caskroom/coqide/8.4pl5/CoqIDE_8.4pl5.app/Contents/MacOS
export PATH=~/.naphatkrit_bin:$PATH

# Make the command line history ignore duplicate command lines.
history_control=ignoredups

 # Establish a safe environment.
 set -o ignoreeof         # Do not log out with <ctrl-d>
 #set -o noclobber         # Do not overwrite files via '>'
 alias rm="rm -i"         # Prompt before removing files via rm
 alias cp="cp -i"         # Prompt before overwriting files via cp
 alias mv="mv -i"         # Prompt before overwriting files via mv

 # Create aliases for commonly used commands.
 alias ls="ls -a"
 alias ll="ls -al"

# make grep show file and line number
alias grep="grep -Hn"

# Make the sort program behave in the traditional way.
export LC_ALL=C

alias reload="source ~/.bashrc"

# testing
alias t="testscript"

# ssh-agent
SSH_ENV="$HOME/.ssh/environment"

function start_agent {
     echo "Initialising new SSH agent..."
     rm -f "$SSH_ENV"
     /usr/bin/ssh-agent | sed 's/^echo/#echo/' > "${SSH_ENV}"
     echo succeeded
     chmod 600 "${SSH_ENV}"
     . "${SSH_ENV}" > /dev/null
     /usr/bin/ssh-add;
}

# Source SSH settings, if applicable

# if [ -f "${SSH_ENV}" ]; then
#      . "${SSH_ENV}" > /dev/null
#      #ps ${SSH_AGENT_PID} doesn't work under cywgin
#      ps -ef | grep ${SSH_AGENT_PID} | grep ssh-agent$ > /dev/null || {
#          start_agent;
#      }
# else
#      start_agent;
# fi

# must have .fav_dirs in home directory
function fcd
{
    echo 'cd '$(grep -i "^\S*$1" ~/.fav_dirs | awk '(NR==1){print $2}')/$2
    cd $(grep -i "^\S*$1" ~/.fav_dirs | awk '(NR==1){print $2}')/$2
}
function fopen
{
    echo 'open '$(grep -i "^\S*$1" ~/.fav_dirs | awk '(NR==1){print $2}')/$2
    open $(grep -i "^\S*$1" ~/.fav_dirs | awk '(NR==1){print $2}')/$2
}
function fadd
{
    echo $1 $(pwd) >> ~/.fav_dirs
    sort ~/.fav_dirs > ~/.fav_dirs.temp && mv -f ~/.fav_dirs.temp ~/.fav_dirs
}
function fls
{
    cat ~/.fav_dirs
}

DIR=$( cd "$( dirname "${BASH_SOURCE[0]}" )" && pwd )
source $DIR/.git-prompt.sh
# export GIT_PS1_SHOWDIRTYSTATE=1
# export GIT_PS1_SHOWUPSTREAM="auto"
export PS1='\[\e[32m\]\u@\h:\w\[\e[0m\]$(__git_ps1 " \[\e[33m\](%s)\[\e[0m\]") \$ '
# export PROMPT_COMMAND='__git_ps1 "\u@\h:\w" "\\\$ "'


if  command -v brew >/dev/null 2>&1 ; then
    if [ -f $(brew --prefix)/etc/bash_completion ]; then
        . $(brew --prefix)/etc/bash_completion
    fi
fi
