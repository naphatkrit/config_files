# Set name of the theme to load --- if set to "random", it will
# load a random theme each time oh-my-zsh is loaded, in which case,
# to know which specific one was loaded, run: echo $RANDOM_THEME
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="risto"

# Which plugins would you like to load?
# Standard plugins can be found in $ZSH/plugins/
# Custom plugins may be added to $ZSH_CUSTOM/plugins/
# Example format: plugins=(rails git textmate ruby lighthouse)
# Add wisely, as too many plugins slow down shell startup.
# plugins=(git)
# plugins=(git z zsh-autosuggestions zsh-completions)
plugins=(git z zsh-completions)
ZSH_AUTOSUGGEST_STRATEGY=(completion history)

source $ZSH/oh-my-zsh.sh
autoload -U compinit && compinit  # needed to get zsh-completions working

export EDITOR='vim'

# Establish a safe environment
alias rm="rm -i"         # Prompt before removing files via rm
alias cp="cp -i"         # Prompt before overwriting files via cp
alias mv="mv -i"         # Prompt before overwriting files via mv

# NOTE this must come before oh-my-zsh.sh which will call compinit
if type brew &>/dev/null; then
    FPATH=$(brew --prefix)/share/zsh/site-functions:$FPATH
fi

if test -d /usr/local/opt/git/share/git-core/contrib/diff-highlight/; then
    export PATH="$PATH:/usr/local/opt/git/share/git-core/contrib/diff-highlight/"
fi

if test -d /opt/homebrew/share/git-core/contrib/diff-highlight/; then
    export PATH="$PATH:/opt/homebrew/share/git-core/contrib/diff-highlight/"
fi

export PATH="$PATH:$HOME/go/bin"

export PATH="$PATH:/Applications/MacVim.app/Contents/bin"
