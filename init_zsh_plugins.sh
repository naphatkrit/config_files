#!/bin/bash -eu

function clone_or_update
{
    plugin_path="${ZSH_CUSTOM:-$HOME/.oh-my-zsh/custom}/plugins/$1/"
    if [ -d "$plugin_path" ]; then
        echo "Updating $1"
        git -C "$plugin_path" pull --ff-only origin master
    else
        echo "Installing $1"
        git clone $2 "$plugin_path"
    fi
}

clone_or_update zsh-autosuggestions https://github.com/zsh-users/zsh-autosuggestions
clone_or_update zsh-completions https://github.com/zsh-users/zsh-completions
