set nocompatible              " be iMproved, required

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" vim-plug
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Install vim-plug if it doesn't already exist
if empty(glob('~/.vim/autoload/plug.vim'))
  silent !curl -fLo ~/.vim/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif


call plug#begin('~/.vim/plugged')
" My Bundles
" better status bars
Plug 'vim-airline/vim-airline'
Plug 'vim-airline/vim-airline-themes'

" Colors
Plug 'altercation/vim-colors-solarized'

" plugins
" navigating directories
Plug 'scrooloose/nerdtree'

" supertab
Plug 'ervandew/supertab'

" python autocomplete
Plug 'davidhalter/jedi-vim'

" java autocomplete
Plug 'vim-scripts/javacomplete'

" C/C++ autocomplete
Plug 'vim-scripts/OmniCppComplete'
Plug 'mortice/exuberant-ctags'

" go
Plug 'fatih/vim-go', {'tag': 'v1.25'}

" Highlight errors on save
Plug 'vim-syntastic/syntastic'

" snippets
" Plug 'msanders/snipmate.vim'

" fuzzy finder
Plug 'ctrlpvim/ctrlp.vim'

" Replaces grep
Plug 'mileszs/ack.vim'

" Tag Bar
Plug 'majutsushi/tagbar'

" TypeScript plugin
Plug 'leafgarland/typescript-vim'

" less plugin
Plug 'groenewege/vim-less'

call plug#end()

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" General
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" force use of homebrew python for macvim
" if exists("&pythondll") && exists("&pythonhome")
"   if has('python3')
"     command! -nargs=1 Py py3 <args>
"     set pythonthreedll=/usr/local/Frameworks/Python.framework/Versions/3.7/Python
"     set pythonthreehome=/usr/local/Frameworks/Python.framework/Versions/3.7
"   else
"     command! -nargs=1 Py py <args>
"     set pythondll=/usr/local/Frameworks/Python.framework/Versions/2.7/Python
"     set pythonhome=/usr/local/Frameworks/Python.framework/Versions/2.7
"   endif
" endif

" set working directory to current directory
" autocmd BufEnter * if expand("%:p:h") !~ '^/tmp' | silent! lcd %:p:h | endif

" syntax highlighting
syntax enable
syntax on
set grepprg=grep\ -nH\ $*

" code completion

set omnifunc=syntaxcomplete#Complete 
if has("autocmd")
    autocmd Filetype java setlocal omnifunc=javacomplete#Complete
    autocmd Filetype java setlocal completefunc=javacomplete#CompleteParamsInfo
endif

" auto-indent
set autoindent
set smartindent

" soft tab
set expandtab
set smarttab

"" tab width
set tabstop=4
set shiftwidth=4
set softtabstop=4
autocmd Filetype coq setlocal shiftwidth=2
autocmd Filetype ocaml setlocal shiftwidth=2
autocmd Filetype ocaml setlocal softtabstop=2
autocmd Filetype asm setlocal shiftwidth=8
autocmd Filetype asm setlocal softtabstop=8
autocmd Filetype javascript,typescript setlocal shiftwidth=2
autocmd Filetype javascript,typescript setlocal softtabstop=2
autocmd Filetype proto setlocal shiftwidth=2
autocmd Filetype proto setlocal softtabstop=2
" hard tab for makefile
autocmd FileType makefile setlocal noexpandtab

" line numbers
set number

" highlight search items
set hlsearch

" smart case
set smartcase
set ignorecase

" when searching, move cursor as you are typing the search term
set incsearch

" codes that should be here but I don't understand
set lazyredraw
set magic

" rearranging lines
nmap <M-j> mz:m+<cr>`z
nmap <M-k> mz:m-2<cr>`z
vmap <M-j> :m'>+<cr>`<my`>mzgv`yo`z
vmap <M-k> :m'<-2<cr>`>my`<mzgv`yo`z

if has("mac") || has("macunix")
    nmap <D-j> <M-j>
    nmap <D-k> <M-k>
    vmap <D-j> <M-j>
    vmap <D-k> <M-k>
endif

" Set 7 lines to the cursor when scrolling
set so=7

" set wild menu
set wildmenu
set wildmode=longest:full,full

" show the line number on the bar
set ruler

" Display the status line
set laststatus=2

" Height of the command bar
set cmdheight=2

" make backspace behave in a sane manner.
set backspace=indent,eol,start

" Show matching backets
set showmatch
set mat=2

" Folding settings
set foldmethod=indent
set foldnestmax=10
set nofoldenable
set foldlevel=1

" Treat long lines as break lines
map j gj
map <Down> gj
map k gk
map <Up> gk

" Return to the last edit position when opening files
autocmd BufReadPost *
    \ if line("'\"") > 0 && line("'\"") <= line("$") |
    \   exe "normal! g`\"" |
    \ endif
" Open MacVim in fullscreen mode
" if has("gui_macvim")
"     set fuoptions=maxvert,maxhorz
"     au GUIEnter * set fullscreen
" endif

" highlight the 80th column
if has("gui_macvim")
    "highlight ColorColumn  ctermbg=235 guibg=#2c2d27
    set colorcolumn=80
endif

" remove right/left scrollbar
set guioptions-=r
set guioptions-=L

" use ag instead of ack
if executable('ag')
  let g:ackprg = 'ag --vimgrep'
endif

" supertab
" <C-X><C-O>
let g:SuperTabDefaultCompletionType = "context"
" let g:SuperTabContextDefaultCompletionType = "<c-x><c-o>"
" let g:SuperTabMappingForward = '<c-space>'
" let g:SuperTabMappingBackward = '<s-c-space>'

" Syntastic
let g:syntastic_auto_jump = 1
let g:syntastic_ignore_files = ['.*\.tex','.*\.html', '.*\.cc']

" Go
au FileType go nmap <Leader>dd <Plug>(go-def)
au FileType go nmap <Leader>ds <Plug>(go-def-split)
au FileType go nmap <Leader>dv <Plug>(go-def-vertical)
au FileType go nmap <Leader>dt <Plug>(go-def-tab)

" Automatically open nerdtree if no file is opened
" autocmd vimenter * if !argc() | NERDTree | endif
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 0 && !exists("s:std_in") | NERDTree | endif
" Automatically open nerdtree if a directory is opened
autocmd StdinReadPre * let s:std_in=1
autocmd VimEnter * if argc() == 1 && isdirectory(argv()[0]) && !exists("s:std_in") | exe 'NERDTree' argv()[0] | wincmd p | ene | endif

" Nerdtree encoding
set encoding=utf-8
let NERDTreeShowHidden=1

" ctrlp
let g:ctrlp_show_hidden=1
" use git to list files, to ignore binaries etc.
let g:ctrlp_user_command = ['.git', 'cd %s && git ls-files -co --exclude-standard']
" start search from cwd instead of trying to find project root (.git folder)
let g:ctrlp_working_path_mode='a'

let g:syntastic_typescript_tsc_args='-t ES5 -m AMD'
"let g:syntastic_typescript_checkers = ["tslint", "tsc"]
let g:syntastic_typescript_tslint_exec='/usr/local/bin/tslint'

let g:syntastic_cpp_gcc_compiler_options='-Wall -std=c++11'

let g:syntastic_c_gcc_quiet_messages={ "type": "style" }
let g:syntastic_c_make_quiet_messages={ "type": "style" }
let g:syntastic_cpp_gcc_quiet_messages={ "type": "style" }
let g:syntastic_cpp_make_quiet_messages={ "type": "style" }
"let g:syntastic_go_checkers = ['golangci_lint']
"let g:syntastic_go_gometalinter_args = ['--disable-all', '--enable=errcheck']
"let g:syntastic_go_golangci_lint_args = ['--disable-all', '--enable=errcheck']
let g:go_list_type = "quickfix"
let g:syntastic_python_checkers = ['flake8']
let g:syntastic_python_flake8_post_args='--ignore=F401,F811,E124,E203,E221,E501'  " ignore import errors as it's noisy with mypy
" let g:syntastic_python_pylint_post_args='--disable=missing-docstring,unused-import,invalid-name,useless-import-alias,useless-object-inheritance,fixme'

" Merlin (ocaml)
let g:opamshare = substitute(system('opam config var share'),'\n$','','''')
execute "set rtp+=" . g:opamshare . "/merlin/vim"
let g:syntastic_ocaml_checkers=['merlin']
au FileType ocaml call SuperTabSetDefaultCompletionType("<c-x><c-o>")

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Colors
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let &t_Co=256

" color schemes
if has('gui_running')
    colorscheme solarized
    set background=dark
else
endif

" fonts
set gfn=Menlo:h14

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Mappings 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" leader
let mapleader=";"
let g:mapleader=";"
let maplocalleader="."
let g:maplocalleader="."

" NERDTree
map <leader>nn :NERDTreeToggle<cr>
map <leader>nb :NERDTreeFromBookmark
map <leader>nf :NERDTreeFind<cr>

" ctrlp
map <leader>ff :CtrlP<cr>

" clear both NERDTree and ctrlp caches
map <Leader>cc :NERDTreeFocus<cr>R<c-w><c-p>:CtrlPClearCache<cr>

" netrw
map <leader>mm :Vexplore<cr>

" django
map <leader>dj :setfiletype htmldjango<cr>

"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" Autocomplete 
"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

" prérequis tags      
set nocp    

" configure tags - add additional tags here or comment out not-used ones      
set tags+=~/.vim/tags/stl      
set tags+=~/.vim/tags/gl      
set tags+=~/.vim/tags/sdl      
set tags+=~/.vim/tags/qt4      

" build tags of your own project with CTRL+F12      
"map <C-F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<CR>      
noremap <F12> :!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>      
inoremap <F12> <Esc>:!ctags -R --c++-kinds=+p --fields=+iaS --extra=+q .<cr>      

" OmniCppComplete      
let OmniCpp_NamespaceSearch = 1      
let OmniCpp_GlobalScopeSearch = 1      
let OmniCpp_ShowAccess = 1      
let OmniCpp_MayCompleteDot = 1      
let OmniCpp_MayCompleteArrow = 1      
let OmniCpp_MayCompleteScope = 1      
let OmniCpp_DefaultNamespaces = ["std", "_GLIBCXX_STD"]      

" automatically open and close the popup menu / preview window      
au CursorMovedI,InsertLeave * if pumvisible() == 0|silent! pclose|endif      
set completeopt=menuone,menu,longest,preview

" git-sync, if configured
autocmd BufWritePost * silent! execute '! if which git-sync >/dev/null 2>&1 && git config remote.sync.url >/dev/null 2>&1; then git sync push; fi'
